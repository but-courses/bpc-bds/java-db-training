package org.but.feec.db.training;

import org.but.feec.db.training.api.PersonFullDetailViewDto;
import org.but.feec.db.training.api.SQLInjectionExampleDto;
import org.but.feec.db.training.config.DataSourceConfig;
import org.but.feec.db.training.data.dao.PersonAuthView;
import org.but.feec.db.training.data.dao.PersonBasicView;
import org.but.feec.db.training.data.dao.PersonCreateRequestDto;
import org.but.feec.db.training.data.dao.PersonCreateResponseDto;
import org.but.feec.db.training.data.repository.MeetingRepository;
import org.but.feec.db.training.data.repository.PersonRepository;
import org.but.feec.db.training.data.repository.SQLInjectionExampleRepository;
import org.but.feec.db.training.exceptions.EntityNotFoundException;
import org.but.feec.db.training.service.MeetingService;
import org.but.feec.db.training.service.PersonAuthService;
import org.but.feec.db.training.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.UUID;

/**
 * Application entrypoint
 */
public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        logger.info("Main app started.");
        App app = new App();
        DataSourceConfig.initializeDataSource(args);
        PersonRepository personRepository = new PersonRepository();
        PersonService personService = new PersonService(personRepository);
        PersonAuthService personAuthService = new PersonAuthService(personRepository);

        SQLInjectionExampleRepository sqlInjectionExampleRepository = new SQLInjectionExampleRepository();

        try {
            // TODO for all the below mentioned tasks just uncomment essential method calls

            //TODO Task 1 -- Check slide with SQL Injection (try to drop table bds.sql_injection_table1)
            /**
             * Set a string that attacks on a database using SQL Injection to drop table. Refresh the database and check if the database loss a table. Check all the methods that are called through this method chain.
             */
            app.sqlInjectionFindDataDropTable("test", sqlInjectionExampleRepository);

            //TODO Task 2 -- Check prepared statements
            /**
             * Check the code behind the scenes (prepared statements) and add the same SQL Injection string with that you deleted the table in the previous task (same task for another "dummy" table).
             */
            app.safelyFindData("", sqlInjectionExampleRepository);

            // TODO Task 3 -- Check Username/Password flow for that task
            /**
             * Implement User authentication using Argon2 (the passwords stored in the DB are "batman"). Use any user account and add selected username and his password as batman. If user is authenticated print "User is authenticated" if not print "User is not authenticated".
             */
            String userEmailInput = ""; // TODO fill username
            String userPasswordInput = ""; // TODO fill user password
            PersonAuthView personAuthView = personAuthService.findPersonByEmail(userEmailInput);
            // TODO implement isAuthenticated method via Argon2 (check README.md of the library in the presentations)
            if (personAuthView == null) {
                throw new EntityNotFoundException("Given username is not found: " + userEmailInput);
            }
            if (personAuthService.isAuthenticated(userPasswordInput, personAuthView.getPassword())) {
                System.out.println("Authenticated");
            } else {
                System.out.println("Not authenticated, wrong credentials");
            }

            // TODO Task 4 -- Create a new person and hash the password of that new person
            /**
             * Check the methods that are called in the createPersonRequest method and implement the encoding of the password in PersonService class method createPerson
             */
            app.createPersonRequest(personService);
            // TODO Task 5 -- Integrate Logging into the Project
            /**
             * Check /src/main/resources and logback.xml file (especially the rolling features and other configurations -- get if from here: http://logback.qos.ch/manual/appenders.html).
             * Implement info/error/debug logging test (logger.info("...")). Check the logs folder how the log file content looks
             * Stop the DB service and run the app again, you should see the ERROR message in the logs.
             */
            personService.logTest();

            // TODO Task 6 -- Check how the JdbcTemplate is used in the source code (if you want, you can implement additional method that uses JdbcTemplate)
            /**
             * Uncomment the following lines printing all the persons using JdbcTemplate
             */
//            JdbcTemplateConfig jdbcTemplate = new JdbcTemplateConfig();
//            PersonRepositoryJdbcTemplate personRepositoryJdbcTemplate = new PersonRepositoryJdbcTemplate(jdbcTemplate.jdbcTemplate());
//            List<PersonBasicView> persons = personRepositoryJdbcTemplate.findAll();
//            persons.forEach(System.out::println);

            // TODO Task 7 -- Check methods behind the performPersonOperations and implement similar ones with meetingOperations
            //            app.performPersonOperations(personService);
            MeetingRepository meetingRepository = new MeetingRepository();
            MeetingService meetingService = new MeetingService(meetingRepository);
//            app.performMeetingOperations(meetingService);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage(), e);
        }
    }

    /**
     * This method uses preparedStatement its safe.
     */
    private void safelyFindData(String userInput, SQLInjectionExampleRepository sqlInjectionExampleRepository) {
        SQLInjectionExampleDto sqlInjectionExampleDto = sqlInjectionExampleRepository.findByEmailPreparedStatement(userInput);
        System.out.println(sqlInjectionExampleDto);
    }

    /**
     * This method uses pure statement its not safe for userInput data!
     */
    private void sqlInjectionFindDataDropTable(String userInput, SQLInjectionExampleRepository sqlInjectionExampleRepository) {
        SQLInjectionExampleDto sqlInjectionExampleDto = sqlInjectionExampleRepository.findByEmailStatement(userInput);
        System.out.println(sqlInjectionExampleDto);
    }

    private void createPersonRequest(PersonService personService) {
        PersonCreateRequestDto personCreateRequestDto = new PersonCreateRequestDto();
        personCreateRequestDto.setPwd(new char[]{'p', 'w', 'd'});
        personCreateRequestDto.setBirthdate(LocalDate.of(2001, Month.APRIL, 15));
        personCreateRequestDto.setEmail("johndoe@email.cz:" + UUID.randomUUID());
        personCreateRequestDto.setGivenName("John");
        personCreateRequestDto.setFamilyName("Doe");
        personCreateRequestDto.setNickname("Doe");

        PersonCreateResponseDto personCreateResponseDto = personService.createPerson(personCreateRequestDto);
        System.out.println("The following person was created" + personCreateResponseDto);
    }

    private void performPersonOperations(PersonService personService) {
        // find personWithAddressView by id 5
        PersonFullDetailViewDto personWithAddressView = personService.findById(4L); // missing exception handling
        System.out.println("Details of person (including address info) with id 5: " + personWithAddressView);
        System.out.println();

        List<PersonBasicView> persons = personService.findAll();
        for (PersonBasicView p : persons) {
            System.out.println(p);
        }
    }

    private void performMeetingOperations(MeetingService meetingService) {
        // complete implementation
    }

    private void findMoreThanYouWant(String userInput, SQLInjectionExampleRepository sqlInjectionExampleRepository) {
        List<SQLInjectionExampleDto> sqlInjectionExampleDto = sqlInjectionExampleRepository.findByNameStatement(userInput);
        sqlInjectionExampleDto.forEach(System.out::println);
    }

    private void findMoreThanYouWantPreparedStatement(String userInput, SQLInjectionExampleRepository sqlInjectionExampleRepository) {
        List<SQLInjectionExampleDto> sqlInjectionExampleDto = sqlInjectionExampleRepository.findByNamePreparedStatement(userInput);
        sqlInjectionExampleDto.forEach(System.out::println);
    }
}
