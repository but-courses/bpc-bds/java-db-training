package org.but.feec.db.training.data.repository.jdbctemplate;

import org.but.feec.db.training.data.dao.PersonBasicView;
import org.but.feec.db.training.data.dao.PersonContact;
import org.but.feec.db.training.data.dao.PersonWithAddressView;
import org.but.feec.db.training.data.mappers.PersonContactsMapper;
import org.but.feec.db.training.data.mappers.PersonWithAddressMapper;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * The analogous implementation of methods in {see @link org.but.feec.db.training.data.repository.PersonRepository} using JdbcTemplate library
 */
public class PersonRepositoryJdbcTemplate {

    private JdbcTemplate jdbcTemplate;

    public PersonRepositoryJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<PersonBasicView> findAll() {
        return jdbcTemplate.query("SELECT id_person, birthdate, email, given_name, family_name, nickname, id_address FROM bds.person",
                new BeanPropertyRowMapper<>(PersonBasicView.class));
    }

    public PersonWithAddressView findByIdWithAddress(Long id) {
        return jdbcTemplate.queryForObject("SELECT p.id_person, birthdate, email, given_name, family_name, nickname, city, street, house_number, a.id_address" +
                        " FROM bds.person p JOIN bds.address a ON p.id_address = a.id_address WHERE p.id_person = ?",
                new PersonWithAddressMapper(),
                id);
    }

    public List<PersonContact> findPersonContacts(Long id) {
        return jdbcTemplate.query("SELECT contact, title" +
                        " FROM bds.person p JOIN bds.contact c ON p.id_person = c.id_person JOIN bds.contact_type ct ON c.id_contact_type = ct.id_contact_type WHERE p.id_person = ?",
                new PersonContactsMapper(),
                id);
    }

}
