package org.but.feec.db.training.config;

import org.springframework.jdbc.core.JdbcTemplate;

public class JdbcTemplateConfig {

    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(DataSourceConfig.getDataSource());
    }
}
