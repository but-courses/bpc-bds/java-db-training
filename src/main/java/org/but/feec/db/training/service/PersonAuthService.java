package org.but.feec.db.training.service;

import org.but.feec.db.training.data.dao.PersonAuthView;
import org.but.feec.db.training.data.repository.PersonRepository;

public class PersonAuthService {

    private PersonRepository personRepository;

    public PersonAuthService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public PersonAuthView findPersonByEmail(String email) {
        return personRepository.findPersonByEmail(email);
    }


    /**
     * Note: For implementation details see: https://github.com/phxql/argon2-jvm
     *
     * @param userInputPwd User password input
     * @param hashedDbPwd  User password hashed
     * @return return if user is authenticated or not
     */
    public boolean isAuthenticated(String userInputPwd, String hashedDbPwd) {
        throw new java.lang.UnsupportedOperationException("Not implemented yet.");
    }

}
